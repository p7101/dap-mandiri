import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeComponent } from '../home/home.component';
import { FooterComponent } from './footer/footer.component';
import { HighlightCardComponent } from './highlight-card/highlight-card.component';
import { NextStepsComponent } from './next-steps/next-steps.component';
import { ResourcesComponent } from './resources/resources.component';
import { ToolbarComponent } from './toolbar/toolbar.component';

const component = [
  HomeComponent,
  ToolbarComponent,
  HighlightCardComponent,
  ResourcesComponent,
  NextStepsComponent,
  FooterComponent,
]

@NgModule({
  declarations: [...component],
    
  imports: [
    CommonModule
  ],
  exports: [...component]
})
export class DashboardModule { }
